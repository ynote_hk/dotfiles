# ESLint

This is my personal `.eslintrc`. I mainly use it with
[ALE](https://github.com/dense-analysis/ale) and [Vim](https://www.vim.org/).

To use it, go to your home directory and run:
```
ln -s /home/your-home-directory/dotfiles/eslint/.eslintrc.json .
```
