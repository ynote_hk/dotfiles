# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

# Theme
ZSH_THEME="af-magic"

# Enable zsh hooks
autoload -U add-zsh-hook

# Term color
export TERM=screen-256color-bce

# Editor
export EDITOR=vim

# Disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

 # Plugins
plugins=(
  colorize
  command-not-found
  common-aliases
  docker
  gem
  git
  gitfast
  history-substring-search
  last-working-dir
  node
  nvm
  pyenv
  rbenv
  ruby
  ssh-agent
  tmux
)

# Local configuration
export PATH="$HOME/bin:$PATH"
if [ -f ~/.zshlocal ]; then
    source ~/.zshlocal
else
    echo 'File not found: ~/.zshlocal'
fi

# Aliases
if [ -f ~/.zshaliases ]; then
    source ~/.zshaliases
else
    echo 'File not found: ~/.zshaliases'
fi

#toggleInlineStyle ZSH conf
source $ZSH/oh-my-zsh.sh

# Encoding stuff for the terminal
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

# Tmux autocompletion
source ~/dotfiles/bin/tmuxinator.zsh
