## Table of contents

- [Prerequisittes](#prerequisites)
- [Install NeoVim](#install-neovim)
- [Setup](#setup)
- [Plugins](#plugins)
  - [Requirements](#requirements)
  - [Quick start](#quick-start)
- [denite.vim plugin](#denitenvim-plugin)
  - [Requirements](##requirements-1)
  - [Compile Vimproc](#compile-vimproc)
  - [Install Ag](#install-ag)
  - [Why using Ag?](#why-using-ag)
  - [Issue with Vim and Vimproc on MacOS
    Mojave](#issue-with-vim-and-vimproc-on-macos-mojave)
- [copilot.vim plugin](#copilotvim-plugin)
- [Custom cheat sheet](#custom-cheat-sheet)
  - [Commands](#commands)
  - [Understanding `.vimrc`](#understanding-vimrc)

## Prerequisites

By default, vim is often installed with a Linux distribution. This setup
is targeted for [Neovim =>0.4.0](https://github.com/neovim/neovim) with the Python 3
interpreter.

If you don't want to use `denite.nvim` plugin, you can comment the plugin
in the configuration file and use [Vim](http://www.vim.org/download.php)
(v7.4.7 or more recent).

## Install Neovim

On [Debian
distribution](https://github.com/neovim/neovim/wiki/Installing-Neovim#debian),
install the latest version of Neovim [via
appimage](https://github.com/neovim/neovim/releases):
- Download the latest `nvim.appimage` in a `.nvim` directory in your home.
- In this directory, run:
```
chmod u+x nvim.appimage
./nvim.appimage --appimage-extract
./squashfs-root/usr/bin/nvim
```

Create a symbolic link to the binary:
```
sudo ln -s /home/[user]/.nvim/squashfs-root/usr/bin/nvim /usr/local/bin
sudo ln -s /home/[user]/.nvim/squashfs-root/usr/bin/nvim /usr/local/bin/vim
```

Check that you are using Neovim when you run vim:
```
which vim
```

If you are not using Neovim, you should remove the vim package from your
environment.

## Setup

My configuration comes from a [previous migration from
vim](https://neovim.io/doc/user/nvim.html#nvim-from-vim).


Create a symlink .vim -> dotfiles/vim
```sh
ln -s ~/dotfiles/vim ~/.vim
```


Create a symlink .vimrc -> dotfiles/vim/.vimrc
```sh
ln -s ~/dotfiles/vim/.vimrc ~/.vimrc
```

Create a backup folder:
```
mkdir ~/.vim/backup
```

Create a `~/.config/nvim/init.vim` with the following content:
```
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc
```
It will use your `.vimrc` to configure Neovim.

## Plugins

Use [dein.vim](https://github.com/Shougo/dein.vim) to install bundles and
plugins.

### Requirements
- Vim 8.0 or above or NeoVim
- `git` command in $PATH (for fugitive plugin)
- TypeScript global install (for Tsuquyomi plugin)

Please check specific requirements for `denite.nvim` or disable `denite.nvim` in
the configuration file.

### Quick start

Create a folder `~/.config/nvim/dein`:
```sh
mkdir ~/.config/nvim/dein
```

Install dein.vim:
```sh
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh >
installer.sh

sh ./installer.sh ~/.config/nvim/dein
```

Open vim and install `vimproc` with its build directly:
```sh
call dein#add('Shougo/vimproc.vim', {'build' : 'make'})
```

(cf. https://github.com/Shougo/vimproc.vim#deinvim)

Then, install plugins listed in the `.vimrc`:
```sh
:call dein#install()
```

## denite.nvim plugin

### Requirements

Python =>3.6.1:
```
pyenv install 3.6.1
```

[MessagePack for Python](https://github.com/msgpack/msgpack-python):
```
pip install msgpack
```

[pynvim](https://github.com/neovim/pynvim):
```
pip3 install pynvim
```

### Compile Vimproc

If you just install Vimproc plugin, you will need to compile it to use it with
Unite.

```sh
$ cd ~/dotfiles/vim/bundles/repos/github.com/Shougo/vimproc.vim
$ make
```

### Install Ag

In order to use my `.vimrc` with denite.nvim, you'll need to install `ag`.

On macOS:
```
brew install the_silver_searcher
```

On Debian >= 8 (Jessie):
```
apt-get install silversearcher-ag
```

More information on: https://github.com/ggreer/the_silver_searcher.

### Why using Ag?

denite.nvim `file/rec` function can be very slow. I configure denite.nvim so it
uses `ag` to search through my files.

The `ag` command ignores the files that are listed in `.gitignore` and `.ignore`
files.

Thanks to [Inside's blog](http://insidesblog.blogspot.fr/2013/07/unitevim-and-many-files-in-project.html) for this precious improvement.

### Issue with Vim and Vimproc on MacOS Mojave

If you install correctly Vimproc and Unite and you still have this error while
using Unite in Vim:
```
vimproc plugin is not installed.
```

You probably have an issue with the Vim version you are using. As far as I
understand it, the built-in version of Apple provided with Mojave is broken and
use the `python/dyn` library instead of the `python3` library. Check it with
`vim --version`.

To fix it, install the brew version of Vim:
```sh
brew install vim
```

Brew should have create a symlink under the path `/usr/local/bin/vim`. Make sure
`/usr/local/bin` is set in your PATH to enable your machine to use the freshly
installed version of Vim.

## copilot.vim plugin

[GitHub Copilot](https://github.com/github/copilot.vim) uses OpenAI Codex to
suggest code and entire functions in real-time right. To use it with Neovim,
you need:
- NeoVim => v0.9.0
- Node >= v16, <v18

Install the plugin:
```
:call dein#install()
```

Setup the credentials with:
```
:Copilot setup
```

## Custom cheat sheet

### Commands

| Commands | Description |
|----------|-------------|
| `Ctrl` + `n` | Open the current directory tree |
| `,` + `a` + `=` | Align all the `=` character in the selection |
| `,` + `a` + `:` | Align all the `,` character in the selection |
| `,` + `g` + `space` | Grep the selection and opens a pane with the grep result |
| `,` + `f` | Search a file by name in the current directory |

### Understanding `.vimrc`

- `<C-n>` means to use the key `Ctrl` and then, the key `n`
- `<Leader>` means to use `,`.
- `let g:foo = 'bar'` means setting `bar` to `foo` as a global variable.
- `map` is used to map a key in general.
- `nmap` is used to map a key in normal mode.
- `imap` is used to map a key in insert mode.
- `vmap` is used to map a key in visual mode.
