" Sections:
"    -> General
"    -> VIM user interface
"    -> Colors and Fonts
"    -> Files and backups
"    -> Text, tab and indent related
"    -> Moving around, tabs and buffers
"    -> Custom mappings
"    -> Plugins configuration

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

filetype plugin on

set nocompatible                                              " Makes vim behave in a more useful way than vi
set number                                                    " Line numbers
set ttyfast                                                   " Improve drawing
set lazyredraw                                                " Do not redraw while running macros
set autoread                                                  " Set to auto read when a file is changed from the outside
set bs=2                                                      " Fix backspace issues
set list                                                      " See invisible characters

let mapleader = ","                                           " With a map leader it's possible to do extra key combinations

" Memory
set history=1000
set undolevels=1000
set omnifunc=syntaxcomplete#Complete

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Scrolling
set scrolloff=3
set scrolljump=3

" Wild menu
set wildmenu
set wildmode=list:longest,full

" Visual options
set ruler                                                     " Show line cursor info
set cursorline
set cmdheight=1                                               " Command bar height
set linebreak

" For at least vim >= 7.4.338
if v:version > 704 || v:version == 704 && has('patch338')
  set breakindent
endif

" Set backspace configuration
set ww=b,s,<,>

set hlsearch                                                  " Highlight search words
set ignorecase                                                " Ignore search case
"set incsearch                                                 " Make search act like search in modern browsers
set magic                                                     " For regular expressions
set showmatch                                                 " Show matching braces when text indicator is over them

" No sound on errors
set noerrorbells
set novisualbell
set t_vb=

set grepprg=git\ grep\ -n\ $*

" Character to precede line wraps
" Using let instead of set to be able to use quotes
set showbreak=↪
let &showbreak="\u21aa "

" Folding
set foldmethod=indent
set foldlevel=20

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

syntax on                                                     " Syntax coloration
colorscheme darkburn
set encoding=utf-8                                             " Characters encoding
set ffs=unix,dos,mac                                          " Set folding format to prevent from bad carriage return

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files and backups
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Backup on
set backup
set writebackup
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//

" Backup off
"set nobackup
"set nowb
"set noswapfile

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Indentation
set autoindent                                                " Auto indent
set smartindent                                               " Smart indent
"set nowrap                                                    " No wrap lines

" Tabs
set softtabstop=2                                             " Tab width
set tabstop=2                                                 " Tab width
set shiftwidth=2                                              " Indent width
set expandtab                                                 " Use whitespace instead of tab

" Columns
set tw=80
set formatoptions+=t

if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Switch to the next buffer
nmap <Tab> :bn<Enter>
" Switch to the previous buffer
nmap <S-Tab> :bp<Enter>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Custom Mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Disable Arrow keys in Normal mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Stop auto-indenting when paste
nnoremap <silent> yo  :set paste<cr>o
nnoremap <silent> yO  :set paste<cr>O

" Select the last pasted text
" http://vim.wikia.com/wiki/Selecting_your_pasted_text
nnoremap <expr> gp '`[' . strpart(getregtype(), 0, 1) . '`]'

" Toggle quickfix buffer
nnoremap <leader>q :call QuickfixToggle()<cr>

let g:quickfix_is_open = 0

function! QuickfixToggle()
    if g:quickfix_is_open
        cclose
        let g:quickfix_is_open = 0
        execute g:quickfix_return_to_window . "wincmd w"
    else
        let g:quickfix_return_to_window = winnr()
        copen
        let g:quickfix_is_open = 1
    endif
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""
" dein.vim "
""""""""""""

" Ward off unexpected things that your distro might have made, as
" well as sanely reset options when re-sourcing .vimrc
set nocompatible

" Set Dein base path (required)
" let s:dein_base = '/opt/data/home/fanny/.local/share/dein'
let s:dein_base = '/home/fanny/.cache/dein'

" Set Dein source path (required)
" let s:dein_src = '/opt/data/home/fanny/.local/share/dein/repos/github.com/Shougo/dein.vim'
let s:dein_src = '/home/fanny/.cache/dein/repos/github.com/Shougo/dein.vim'

" Set Dein runtime path (required)
execute 'set runtimepath+=' . s:dein_src

" Call Dein initialization (required)
call dein#begin(s:dein_base)

call dein#add(s:dein_src)

" Add my plugins

" set GitHub API token to enable dein.vim to check update
let g:dein#install_github_api_token='69b5fe6a49bbd06734055e3b70e20497b46fa64d'

" Asynchronous execution library. Useful for some plugins.
call dein#add('Shougo/vimproc.vim', {'build' : 'make'})

" Plugins for dein.vim and denite.nvim
call dein#add('Shougo/deoplete.nvim')
if !has('nvim')
call dein#add('roxma/nvim-yarp')
call dein#add('roxma/vim-hug-neovim-rpc')
endif

""""""""""""""
" Appearance "
""""""""""""""

" Status tabline
call dein#add('bling/vim-airline')

" Git window
"   My use: :G [git command]
call dein#add('tpope/vim-fugitive')

" Grep command with git and quickfix window for file match navigation
"   My use: <leader>g<space> on visual selection
call dein#add('inside/vim-grep-operator')

" File system explorer
"   My use: <C-n> to toggle NERDTree window
call dein#add('scrooloose/nerdtree')

" Locate the cursor position in the active windor. Enhance 'cursorline' option
call dein#add('vim-scripts/CursorLineCurrentWindow')

" Locate cursor after a search
call dein#add('inside/vim-search-pulse')

" Highlight trailing whitespace in red
call dein#add('bronson/vim-trailing-whitespace')

""""""""""""""""
" Code editing "
""""""""""""""""

" Fuzzy finder
" My use: <leader>f<space> to open a file search buffer
call dein#add('Shougo/denite.nvim')

" Automatic closing characters (quotes, parenthesis, brackets, etc.)
call dein#add('Raimondi/delimitMate')

" Autocomplete
"   My use: <Tab> in insert mode
call dein#add('ackyshake/VimCompletesMe')

" Autoclose tag
call dein#add('alvan/vim-closetag')

" Comment code easily
"   My use: <leader>c<space> to toggle comments
call dein#add('scrooloose/nerdcommenter')

" Working with variant of a word
" My use:
" - coerce to camel case: <c><r><s>
" - coerce to snake case: <c><r><->
call dein#add('tpope/vim-abolish')

" Vim motion command with Camel Case words
"   My use: <leader>[motion command]
call dein#add('bkad/CamelCaseMotion')

" Code and functions suggestions and autocomplete
call dein#add('github/copilot.vim')

""""""""""""""
" Code style "
""""""""""""""

" General syntax
call dein#add('dense-analysis/ale')

" JavaScript syntax
call dein#add('pangloss/vim-javascript')

" TypeScript syntax
call dein#add('leafgarland/typescript-vim')

" SCSS syntax
call dein#add('cakebaker/scss-syntax.vim')

" Markdown syntax
call dein#add('godlygeek/tabular') " tabular is needed for vim-markdown
call dein#add('plasticboy/vim-markdown')

" Liquid syntax
call dein#add('tpope/vim-liquid')

" Svelte syntax
call dein#add('othree/html5.vim')
call dein#add('evanleck/vim-svelte')

" Finish Dein initialization (required)
call dein#end()

" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
if has('filetype')
  filetype indent plugin on
endif

" Enable syntax highlighting
if has('syntax')
  syntax on
endif

" Uncomment if you want to install not-installed plugins on startup.
" if dein#check_install()
 " call dein#install()
" endif

"""""""""""""""
" delimitMate "
"""""""""""""""

" Expand carriage return
let delimitMate_expand_cr = 1

""""""""""""""""""
" NERD Commenter "
""""""""""""""""""

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

"""""""""""""
" NERD Tree "
"""""""""""""
map <C-n> :NERDTreeToggle<CR>

"""""""""""""""""""""
" vim-grep-operator "
"""""""""""""""""""""
nmap <leader>g <Plug>GrepOperatorOnCurrentDirectory
vmap <leader>g <Plug>GrepOperatorOnCurrentDirectory
nmap <leader><leader>g <Plug>GrepOperatorWithFilenamePrompt
vmap <leader><leader>g <Plug>GrepOperatorWithFilenamePrompt
let g:grep_operator = 'Ag'
let g:grep_operator_set_search_register = 1

"""""""""""""""
" denite.nvim "
"""""""""""""""

" Mapping key
nnoremap <leader>f :<C-u>Denite -split=no -start-filter file/rec<CR>

" Define mappings
autocmd FileType denite call s:denite_my_settings()
function! s:denite_my_settings() abort
  nnoremap <silent><buffer><expr> <CR>
  \ denite#do_map('do_action')
  nnoremap <silent><buffer><expr> d
  \ denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr> p
  \ denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> q
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> i
  \ denite#do_map('open_filter_buffer')
  nnoremap <silent><buffer><expr> <Space>
  \ denite#do_map('toggle_select').'j'
endfunction

autocmd FileType denite-filter call s:denite_filter_my_settings()
function! s:denite_filter_my_settings() abort
  imap <silent><buffer> <C-o> <Plug>(denite_filter_quit)
endfunction

call denite#custom#var('file/rec', 'command',
	\ ['ag', '--follow', '--nocolor', '--nogroup', '-g', ''])

" Change ignore_globs
call denite#custom#filter('matcher/ignore_globs', 'ignore_globs',
      \ [ '.git/', '.ropeproject/', '__pycache__/', '.idea/',
      \   'venv/', 'images/', '*.min.*', 'img/', 'fonts/', 'node_modules/'])

" For the file/rec command, ignore ignore_globs,
" only match project file and fuzzy string
call denite#custom#source(
	\ 'file/rec', 'matchers',
  \ ['matcher/fuzzy', 'matcher/project_files', 'matcher/ignore_globs'])

"""""""""""""""
" vim airline "
"""""""""""""""

" enable vim-airline without splitting vim
set laststatus=2

let g:airline_powerline_fonts=0
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.linenr = '¶ '
let g:airline#extensions#ale#enabled = 1

""""""""""""""""""
" VimCompletesMe "
""""""""""""""""""
let b:vcm_tab_complete = 'omni'

"""""""
" Ale "
"""""""
let g:ale_sign_error = "🍁"
let g:ale_sign_warning = "🍂"
let g:ale_sign_info = "🍃"
let g:ale_virtualtext_cursor = 1
let g:ale_virtualtext_prefix = "✨ "
highlight ALEErrorSign ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
let g:ale_linters = {
\   'javascript': ['prettier', 'eslint'],
\   'typescript': ['prettier', 'eslint'],
\   'ruby': ['rubocop'],
\}

let g:ale_fixers = {
\  'javascript': ['prettier', 'eslint'],
\  'typescript': ['prettier', 'eslint'],
\}

let g:ale_fix_on_save = 1

"""""""""""""""
" vim Svelte "
"""""""""""""""
let g:svelte_preprocessors = ['typescript']
let g:svelte_indent_script = 0
let g:svelte_indent_style = 0

"""""""""""""""""""
" CamelCaseMotion "
"""""""""""""""""""
call camelcasemotion#CreateMotionMappings('<leader>')

"""""""""""""""
" autocommand "
"""""""""""""""
augroup mygroup
" clear the group's autocommand
    autocmd!

    " Disables paste mode when leaving insert mode
    autocmd InsertLeave *
                \ if &paste == 1 |
                \     set nopaste |
                \ endif

    " remove trailing space when saving file
    autocmd BufWritePre * :FixWhitespace

    " enable autocomplete by filetype
    autocmd FileType html set omnifunc=htmlcomplete#CompleteTags

    " Set scss file filetype
    au BufRead,BufNewFile *.scss set filetype=scss

    " Set ERB file filetype to Ruby
    au BufRead,BufNewFile *.html.erb setfiletype ruby

    " Set html filetype to html twig to enable lowercase autocomplete
    au BufRead,BufNewFile *.html setfiletype html.twig

    " Only close '(', '{', '[' in html files
    au FileType html.twig let b:delimitMate_matchpairs = "(:),[:],{:}"
    au FileType html.twig let b:delimitMate_autoclose = 0
augroup END

set secure
