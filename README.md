# Dotfiles

This is my personal setup for tools that I use every day on my machine.

## Basics

- [Current machine
  setup](https://gitlab.com/ynote_hk/dotfiles/-/blob/main/docs/totoro-debian-9-setup.md)
- [Vim](https://gitlab.com/ynote_hk/dotfiles/-/tree/main/vim)
- [Zsh](https://gitlab.com/ynote_hk/dotfiles/-/tree/main/zsh)
- [Tmux](https://gitlab.com/ynote_hk/dotfiles/-/tree/main/tmux)

## If you want more...

You can install the following packages and check the matching directory in this
repository:
* [Oh My Zsh](https://github.com/robbyrussell/oh-my-zsh)
* [Irssi](https://irssi.org/download/)
* [Tmuxinator](https://github.com/tmuxinator/tmuxinator)
